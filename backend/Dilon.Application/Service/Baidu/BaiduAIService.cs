﻿using Baidu.Aip.Ocr;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dilon.Application.Service
{
    /// <summary>
    /// 百度AI接口
    /// </summary>
    [ApiDescriptionSettings(Name = "BaiduAI", Order = 999)]
    [AllowAnonymous]
    public class BaiduAIService:IBaiduAIService, IDynamicApiController, ITransient
    {
        public string AppId = "14808844";
        public string ApiKey = "sKZh9G4Ecqt2B44ok68MQ3IL";
        public string SecretKey = "VQQIPFPmnD6yrKVOMwwjXkGGeRjy7LCQ ";
        public Ocr client;
        public BaiduAIService(IConfiguration configuration ) { 
            AppId= configuration["Open:Baidu:app_id"];
            ApiKey = configuration["Open:Baidu:app_key"];
            SecretKey = configuration["Open:Baidu:secret_key"];
            client= new Baidu.Aip.Ocr.Ocr(ApiKey, SecretKey) { Timeout = 60000 };
        }

        public JObject Accurate(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 通用文字识别（高精度版）
        /// </summary>
        /// <param name="image"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public JObject AccurateBasic(byte[] image, Dictionary<string, object> options = null)
        {  
            //var client =  
            try
            {
                //var imagebyte = System.IO.File.ReadAllBytes(image);
                var result =  client.AccurateBasic(image, options);
                // Console.WriteLine("通用文字识别（高精度版）:");
                // Console.WriteLine(result.ToString()); 
               
                return result;
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex.ToString());
                return null;// ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image">图片路径</param>
        /// <param name="options"></param>
        /// <returns></returns>
        public JObject AccurateBasic(string image, Dictionary<string, object> options = null)
        {
            //var client =  
            try
            {
                var imagebyte = System.IO.File.ReadAllBytes(image);
                var result = client.AccurateBasic(imagebyte, options);
                // Console.WriteLine("通用文字识别（高精度版）:");
                // Console.WriteLine(result.ToString()); 

                return result;
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex.ToString());
                return null;// ex.Message;
            }
        }

        public JObject AirTicket(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Bankcard(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject BirthCertificate(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject BusinessCard(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject BusinessLicense(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Custom(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject DrivingLicense(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Form(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject General(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject GeneralBasic(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject GeneralBasicUrl(string url, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject GeneralEnhanced(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject GeneralEnhancedUrl(string url, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject GeneralUrl(string url, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Handwriting(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject HkMacauExitentrypermit(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject HouseholdRegister(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Idcard(byte[] image, string idCardSide, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject InsuranceDocuments(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Invoice(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject LicensePlate(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Lottery(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Numbers(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Passport(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Qrcode(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject QuotaInvoice(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject Receipt(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject TableRecognition(byte[] image, long timeoutMiliseconds = 20000, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject TableRecognitionGetResult(string requestId, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject TableRecognitionRequest(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject TableRecognitionToExcel(byte[] image, long timeoutMiliseconds = 20000, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject TableRecognitionToJson(byte[] image, long timeoutMiliseconds = 20000, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject TaiwanExitentrypermit(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject TaxiReceipt(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject TrainTicket(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject VatInvoice(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject VehicleCertificate(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject VehicleInvoice(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject VehicleLicense(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject VinCode(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject WebImage(byte[] image, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }

        public JObject WebImageUrl(string url, Dictionary<string, object> options = null)
        {
            throw new NotImplementedException();
        }
    }
}
