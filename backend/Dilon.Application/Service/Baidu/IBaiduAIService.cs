﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dilon.Application.Service
{
    public interface IBaiduAIService
    {
        JObject Accurate(byte[] image, Dictionary<string, object> options = null);
        JObject AccurateBasic(byte[] image, Dictionary<string, object> options = null);
        JObject AirTicket(byte[] image, Dictionary<string, object> options = null);
        JObject Bankcard(byte[] image, Dictionary<string, object> options = null);
        JObject BirthCertificate(byte[] image, Dictionary<string, object> options = null);
        JObject BusinessCard(byte[] image, Dictionary<string, object> options = null);
        JObject BusinessLicense(byte[] image, Dictionary<string, object> options = null);
        JObject Custom(byte[] image, Dictionary<string, object> options = null);
        JObject DrivingLicense(byte[] image, Dictionary<string, object> options = null);
        JObject Form(byte[] image, Dictionary<string, object> options = null);
        JObject General(byte[] image, Dictionary<string, object> options = null);
        JObject GeneralBasic(byte[] image, Dictionary<string, object> options = null);
        JObject GeneralBasicUrl(string url, Dictionary<string, object> options = null);
        JObject GeneralEnhanced(byte[] image, Dictionary<string, object> options = null);
        JObject GeneralEnhancedUrl(string url, Dictionary<string, object> options = null);
        JObject GeneralUrl(string url, Dictionary<string, object> options = null);
        JObject Handwriting(byte[] image, Dictionary<string, object> options = null);
        JObject HkMacauExitentrypermit(byte[] image, Dictionary<string, object> options = null);
        JObject HouseholdRegister(byte[] image, Dictionary<string, object> options = null);
        JObject Idcard(byte[] image, string idCardSide, Dictionary<string, object> options = null);
        JObject InsuranceDocuments(byte[] image, Dictionary<string, object> options = null);
        JObject Invoice(byte[] image, Dictionary<string, object> options = null);
        JObject LicensePlate(byte[] image, Dictionary<string, object> options = null);
        JObject Lottery(byte[] image, Dictionary<string, object> options = null);
        JObject Numbers(byte[] image, Dictionary<string, object> options = null);
        JObject Passport(byte[] image, Dictionary<string, object> options = null);
        JObject Qrcode(byte[] image, Dictionary<string, object> options = null);
        JObject QuotaInvoice(byte[] image, Dictionary<string, object> options = null);
        JObject Receipt(byte[] image, Dictionary<string, object> options = null);
        JObject TableRecognition(byte[] image, long timeoutMiliseconds = 20000, Dictionary<string, object> options = null);
        JObject TableRecognitionGetResult(string requestId, Dictionary<string, object> options = null);
        JObject TableRecognitionRequest(byte[] image, Dictionary<string, object> options = null);
        JObject TableRecognitionToExcel(byte[] image, long timeoutMiliseconds = 20000, Dictionary<string, object> options = null);
        JObject TableRecognitionToJson(byte[] image, long timeoutMiliseconds = 20000, Dictionary<string, object> options = null);
        JObject TaiwanExitentrypermit(byte[] image, Dictionary<string, object> options = null);
        JObject TaxiReceipt(byte[] image, Dictionary<string, object> options = null);
        JObject TrainTicket(byte[] image, Dictionary<string, object> options = null);
        JObject VatInvoice(byte[] image, Dictionary<string, object> options = null);
        JObject VehicleCertificate(byte[] image, Dictionary<string, object> options = null);
        JObject VehicleInvoice(byte[] image, Dictionary<string, object> options = null);
        JObject VehicleLicense(byte[] image, Dictionary<string, object> options = null);
        JObject VinCode(byte[] image, Dictionary<string, object> options = null);
        JObject WebImage(byte[] image, Dictionary<string, object> options = null);
        JObject WebImageUrl(string url, Dictionary<string, object> options = null);
    }
}
