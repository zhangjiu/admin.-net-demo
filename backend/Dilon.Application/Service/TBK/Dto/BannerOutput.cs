﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dilon.Application.Service.TBK.Dto
{
   public  class BannerOutput
    {
        public string image { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string location { get; set; }
    }
}
