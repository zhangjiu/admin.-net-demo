﻿using System.Threading.Tasks;

namespace Dilon.Application.Service.TBK
{
    public interface ITbkServices
    {
        string GetIP();

        Task<dynamic> GetBanner(string location = "top");
        dynamic GetActivityInfo(string ActivityId);
    }
}