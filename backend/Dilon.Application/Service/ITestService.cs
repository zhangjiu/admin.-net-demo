﻿namespace Dilon.Application
{
    public interface ITestService
    {
        string GetDescription();
    }
}