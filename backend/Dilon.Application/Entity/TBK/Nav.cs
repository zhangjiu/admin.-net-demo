﻿using Dilon.Core;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dilon.Application.Entity.TBK
{
    [Table("Tbk_Nav")]
    [Comment("导航频道")]
   public class Nav : DEntityBase
    {
        /// <summary>
        /// 频道ID
        /// </summary>
        [Comment("频道ID")]
        
        public virtual int MID { get; set; }
        /// <summary>
        /// 频道名称
        /// </summary>
        [Comment("频道名称")]
        [MaxLength(50)]
        public virtual string Name { get; set; }
        /// <summary>
        /// 频道类型
        /// </summary>
        [Comment("频道类型")]
        public virtual int Type { get; set; }
    }
}
