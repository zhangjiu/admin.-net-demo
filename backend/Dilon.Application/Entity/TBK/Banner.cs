﻿using Dilon.Core;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dilon.Application.Entity.TBK
{
    [Table("Tbk_Banner")]
    [Comment("广告")]
   public class Banner : DEntityBase
    {
        /// <summary>
        /// banner位置
        /// </summary>
        [Comment("banner位置")]
        [MaxLength(50)]
        public virtual string Location { get; set; }
        /// <summary>
        /// Banner名称
        /// </summary>
        [Comment("Banner名称")]
        [MaxLength(50)]
        public virtual string Title { get; set; }
        /// <summary>
        /// Banner链接
        /// </summary>
        [Comment("Banner链接")]
        [MaxLength(2000)]
        public string Url { get; set; } 
        /// <summary>
        /// Banner图片链接
        /// </summary>
        [Comment("Banner图片链接")]
        [MaxLength(2000)]
        public string ImageUrl { get; set; }
    }
}
