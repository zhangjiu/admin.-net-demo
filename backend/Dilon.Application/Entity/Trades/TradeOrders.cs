﻿using Dilon.Core;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dilon.Application.Entity.Trades
{

    /// <summary>
    /// 代码生成实体事例（EF）
    /// </summary>
    [Table("TradeOrders")]
    [Comment("订单数据")]
    public class TradeOrders : DEntityBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Comment("订单名称")]
        [MaxLength(50)]
        public virtual string OrderNo { get; set; }
        /// <summary>
        /// 产品详情
        /// </summary>
        [Comment("产品详情")]
        [MaxLength(50)]
        public virtual string ProductSummary { get; set; }
        /// <summary>
        /// 产品详情
        /// </summary>
        [Comment("订单金额")] 
        public virtual decimal GoodsAmount { get; set; }


    }
}
